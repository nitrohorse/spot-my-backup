const config = {
	// uri: 'http://127.0.0.1:5500',
	uri: 'https://spotmybackup.netlify.app',
	// redirect_uri: 'http://127.0.0.1:5500/login.html',
	redirect_uri: 'https://spotmybackup.netlify.app/login.html',
	client_id: 'ea01ee076c91444a9d04f4866b11e955',
	slowdown_import: 100,
	slowdown_export: 100
};
