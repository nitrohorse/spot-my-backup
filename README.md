# [SpotMyBackup](https://spotmybackup.netlify.app/)

[![Netlify Status](https://api.netlify.com/api/v1/badges/53b6211b-cbe6-4366-b443-ad7d8c3f929d/deploy-status)](https://app.netlify.com/sites/spotmybackup/deploys)

_Forked from https://github.com/secuvera/SpotMyBackup._

Backup and restore your Spotify playlists and "My Music."

This JavaScript-based single-page application allows you to backup all your playlists and import them into any other Spotify account. It uses the OAuth [Implicit Grant Flow of Spotify](https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow) to be able to handle your personal playlists.

![Implicit Grant Flow](AuthG_ImplicitGrant.png)

In consequence, no credentials or data is stored or processed on the Webserver itself.

The data available to me on https://developer.spotify.com/dashboard/applications if you login to https://spotmybackup.netlify.app is exactly:
- Number of daily active users
- Number of users per country
- Number of requests per endpoint used in index.html
- Number of daily requests

You can use SpotMyBackup at https://spotmybackup.netlify.app or on your own webserver (see [Q&A](https://gitlab.com/nitrohorse/spot-my-backup/-/wikis/SpotMyBackup-FAQ)).
